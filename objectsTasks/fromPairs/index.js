'use strict';
// https://ru.hexlet.io/challenges/js_objects_from_pairs/instance
/* Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход массив,
состоящий из массивов-пар и возвращает объект, полученный из этих пар.
Примечания
Если при конструировании объекта попадаются совпадающие ключи, то берётся значение из последнего массива-пары:
fromPairs([['cat', 5], ['dog', 6], ['cat', 11]])
// { 'cat': 11, 'dog': 6 }
Примеры
fromPairs([['fred', 30], ['barney', 40]]);
// { 'fred': 30, 'barney': 40 } */
{
  const fromPairs = (array) => {
    let res = {};
    for (let [key, val] of array) {
      res[key] = val;
    }
    return res;
  };
  //////////////////////////version 2
  const fromPairs2 = (array) =>
    array.reduce((resObj, [key, val]) => {
      return (resObj = { ...resObj, ...{ [key]: val } });
    }, {});

  // fromPairs([
  //   ['cat', 5],
  //   ['dog', 6],
  //   ['cat', 11],
  // ]);
  // fromPairs2([
  //   ['cat', 5],
  //   ['dog', 6],
  //   ['cat', 11],
  // ]);
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_to_rna/instance
/* ДНК и РНК это последовательности нуклеотидов.

Четыре нуклеотида в ДНК это аденин (A), цитозин (C), гуанин (G) и тимин (T).

Четыре нуклеотида в РНК это аденин (A), цитозин (C), гуанин (G) и урацил (U).

Цепь РНК составляется на основе цепи ДНК последовательной заменой каждого нуклеотида:
G -> C
C -> G
T -> A
A -> U
dnaToRna.js
Реализуйте и экспортируйте функцию по умолчанию,
которая принимает на вход цепь ДНК и возвращает соответствующую цепь РНК (совершает транскрипцию РНК).

Если во входном параметре нет ни одного нуклеотида (т.е. передана пустая строка), то функция должна вернуть пустую строку.
Если в переданной цепи ДНК встретится "незнакомый" нуклеотид (не являющийся одним из четырех перечисленных выше),
то функция должна вернуть null.

dnaToRna('CCGTA'); // 'GGCAU'
*/
{
  const dnaToRna = (dna) => {
    let rna = '';
    const handleSwitch = {
      G: 'C',
      C: 'G',
      T: 'A',
      A: 'U',
    };
    for (const nucleotide of dna) {
      if (!handleSwitch[nucleotide]) {
        rna = null;
        break;
      }
      rna += handleSwitch[nucleotide];
    }
    return rna;
  };
  // dnaToRna(''); // ''
  // dnaToRna('ACGTGGTCTTAA'); // 'UGCACCAGAAUU'
  // dnaToRna('ACNTG'); // null
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_query_string/instance
/* Query String (строка запроса) - часть адреса страницы в интернете, содержащая константы и их значения.
Она начинается после вопросительного знака и идет до конца адреса. Пример:

# query string: page=5
https://ru.hexlet.io/blog?page=5
Если параметров несколько, то они отделяются амперсандом &:

# query string: page=5&per=10
https://ru.hexlet.io/blog?per=10&page=5
buildQueryString.js
Реализуйте и экспортируйте функцию по умолчанию, которая принимает на вход список параметров и возвращает сформированный query string из этих параметров:

import bqs from '../buildQueryString.js';

bqs({ param: 'all', param1: true });
// param=all&param1=true
Имена параметров в выходной строке должны располагаться в алфавитном порядке (то есть их нужно отсортировать). */
{
  const bqs = (params) => {
    const keyValOfParams = Object.entries(params).sort();
    const result = keyValOfParams
      .map(([key, val]) => {
        return `${key}=${val}`;
      })
      .join('&');
    return result;
  };
  ///decomposeQueryString
  const dqs = (string) => {
    const result = {};
    const pairs = string.split('&');
    pairs.forEach((pair) => {
      const [key, val] = pair.split('=');
      result[key] = val;
    });
    return result;
  };
  // bqs(dqs('page=1&per=10'));
  // page=1&per=10
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_find_where/instance
/* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход массив (элементы которого — это объекты) 
и пары ключ-значение (тоже в виде объекта), а возвращает первый элемент исходного массива,
значения которого соответствуют переданным парам (всем переданным). Если совпадений не было, то функция должна вернуть null.
 */
{
  const findWhere = (array, params) => {
    let result = null;
    const paramsArr = Object.entries(params);
    loop: for (const elem of array) {
      for (const [key, val] of paramsArr) {
        if (elem[key] !== val) {
          continue;
        } else {
          result = elem;
          break loop;
        }
      }
    }
    return result;
  };

  /* findWhere(
    [
      { title: 'Book of Fooos', author: 'FooBar', year: 1111 },
      { title: 'Cymbeline', author: 'Shakespeare', year: 1611 },
      { title: 'The Tempest', author: 'Shakespeare', year: 1611 },
      { title: 'Book of Foos Barrrs', author: 'FooBar', year: 2222 },
      { title: 'Still foooing', author: 'FooBar', year: 3333 },
      { title: 'Happy Foo', author: 'FooBar', year: 4444 },
    ],
    { author: 'Shakespeare', year: 1611 }
  ); */
  // { title: 'Cymbeline', 'author: 'Shakespeare', 'year: 1611 }
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_scrabble/instance
/* Реализуйте и экспортируйте по умолчанию функцию-предикат,
которая принимает на вход два параметра: набор символов в нижнем регистре (строку) и слово, и проверяет,
можно ли из переданного набора составить это слово. В результате вызова функция возвращает true или false.
При проверке учитывается количество символов, нужных для составления слова, и не учитывается их регистр.

Примеры
scrabble('avjafff', 'java'); // true
scrabble('', 'hexlet'); // false
scrabble('scriptingjava', 'JavaScript'); // true */
{
  const scrabble = (string, word) => {
    let result = true;
    let setOfLetters = string.toLowerCase().split('');
    for (const char of word.toLowerCase()) {
      const index = setOfLetters.indexOf(char);
      if (index === -1) {
        result = false;
        break;
      }
      setOfLetters.splice(index, 1);
    }
    return result;
  };
  //////////////////////////   version vith use  obj

  const createObjList = (str) => {
    const result = {};
    for (const char of str.toLowerCase()) {
      result[char] = (result[char] || 0) + 1;
    }
    return result;
  };

  const scrabble2 = (string, word) => {
    let result = true;
    const stringList = createObjList(string);
    const wordList = createObjList(word);
    for (const char in wordList) {
      if (!stringList[char] || wordList[char] > stringList[char]) {
        result = false;
        break;
      }
    }
    return result;
  };
  // scrabble2('katas', 'steak');
  // scrabble2('avj', 'java'); // false
  // scrabble2('rkqodlw', 'world'); // true
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_operations
/* Реализуйте и экспортируйте по умолчанию функцию, которая сравнивает два объекта и возвращает результат сравнения в виде объекта.
Ключами результирующего объекта будут все ключи из двух входящих объектов,
а значением строка с описанием отличий: added, deleted, changed или unchanged.

Возможные значения:

added — ключ отсутствовал в первом объекте, но был добавлен во второй
deleted — ключ был в первом объекте, но отсутствует во втором
changed — ключ присутствовал и в первом и во втором объектах, но значения отличаются
unchanged — ключ присутствовал и в первом и во втором объектах с одинаковыми значениями
 */
{
  const genDiff = (old, now) => {
    const diff = {};
    for (const key in old) {
      if (now.hasOwnProperty(key))
        diff[key] = old[key] !== now[key] ? 'changed' : 'unchanged';
      else diff[key] = 'deleted';
    }
    for (const key in now) {
      if (!old.hasOwnProperty(key)) diff[key] = 'added';
    }
    return diff;
  };
  /////////////////////////////////////////version2
  const genDiff2 = (old, now) => {
    const diff = {};
    const generalKey = new Set([...Object.keys(old), ...Object.keys(now)]);
    for (const key of generalKey) {
      if (!old.hasOwnProperty(key)) diff[key] = 'added';
      else if (!now.hasOwnProperty(key)) diff[key] = 'deleted';
      else diff[key] = old[key] !== now[key] ? 'changed' : 'unchanged';
    }
    return diff;
  };
  // genDiff(
  //   { one: 'eon', two: undefined, four: true },
  //   { two: 'own', zero: 4, four: true }
  // );
  // {
  //   one: 'deleted',
  //   two: 'changed',
  //   four: 'unchanged',
  //   zero: 'added',
  // }
}
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// https://ru.hexlet.io/challenges/js_objects_to_roman/instance
/* Для записи цифр римляне использовали буквы латинского алфафита: I, V, X, L, C, D, M. Например:

1 обозначалась с помощью буквы I
10 с помощью Х
7 с помощью VII
Число 2020 в римской записи — это MMXX (2000 = MM, 20 = XX).

solution.js
Реализуйте и экспортируйте функцию toRoman(), которая переводит арабские числа в римские.
Функция принимает на вход целое число в диапазоне от 1 до 3000, а возвращает строку с римским представлением этого числа.

Реализуйте и экспортируйте функцию toArabic(), которая переводит число в римской записи в число, записанное арабскими цифрами.
Если переданное римское число не корректно, то функция должна вернуть значение false.

Примеры
toRoman(1);
// 'I'
toRoman(59);
// 'LIX'
toRoman(3000);
// 'MMM'

toArabic('I');
// 1
toArabic('LIX');
// 59
toArabic('MMM');
// 3000

toArabic('IIII');
// false
toArabic('VX');
// false
 */
{
  const romanNums = {
    1000: 'M',
    500: 'D',
    100: 'C',
    50: 'L',
    10: 'X',
    5: 'V',
    1: 'I',
  };
  const subRule = {
    900: 'CM',
    400: 'CD',
    90: 'XC',
    40: 'XL',
    9: 'IX',
    4: 'IV',
  };
  const toRoman = (arabic) => {
    const numbers = [
      ...Object.entries(romanNums),
      ...Object.entries(subRule),
    ].sort((a, b) => b[0] - a[0]);
    let roman = '';
    const print = (count, romanNum) => {
      for (let i = 0; i < count; i++) {
        roman += romanNum;
      }
    };
    for (const [key, value] of numbers) {
      if (arabic / key < 1) continue;
      const count = Math.floor(arabic / key);
      print(count, value);
      arabic = arabic % key;
    }
    return roman;
  };

  const toArabic = (roman) => {
    let result = 0;
    let prevNum = 0;
    const romanNumsRev = Object.fromEntries(
      Object.entries(romanNums).map((el) => {
        el[0] = Number(el[0]);
        return el.reverse();
      })
    ); //replace key & value
    const length = roman.length;
    for (let i = 0; i < length; i++) {
      if (romanNumsRev[roman[i]] < romanNumsRev[roman[i + 1]]) {
        prevNum = romanNumsRev[roman[i]];
      } else {
        result += romanNumsRev[roman[i]] - prevNum;
        prevNum = 0;
      }
    }
    const check = toRoman(result);
    return check === roman ? result : false;
  };

  // toArabic('IIII'); ///false
  toArabic('XXVII'); ///27
  // toRoman(911);
}
