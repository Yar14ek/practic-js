'use strict';

{
  /* Реализуйте функцию buildDefinitionList(), 
которая генерирует HTML список определений (теги <dl>, <dt> и <dd>)
 и возвращает получившуюся строку. При отсутствии элементов в массиве функция возвращает пустую строку. */
  function buildDefinitionList(definitions) {
    const listItem = [];
    for (const row of definitions) {
      listItem.push(`<dt>${row[0]}</dt><dd>${row[1]}</dd>`);
    }
    return definitions.length ? `<dl>${listItem.join('')}</dl>` : '';
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте функцию,
которая заменяет каждое вхождение указанных слов в предложении на последовательность $#%!
и возвращает полученную строку.
Аргументы:
Текст
Набор стоп слов

Словом считается любая непрерывная последовательность символов, включая любые спецсимволы (без пробелов). */
  function makeCensored(string, stopWords) {
    const wordsArr = string.split(' ');
    const newArr = [];
    for (const word of wordsArr) {
      stopWords.includes(word) ? newArr.push('$#%!') : newArr.push(word);
    }
    return newArr.join(' ');
  }

  const sentence2 = 'chicken chicken? chicken! chicken';
  //   makeCensored(sentence2, ['?', 'chicken']);
}
////////////////////////////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте функцию принимающую на вход два массива и
возвращающую количество общих уникальных значений в обоих массивах. */
  function getSameCount(arr1, arr2) {
    const setArr1 = new Set(arr1);
    const setArr2 = new Set(arr2);
    let count = 0;
    setArr1.forEach((el) => (setArr2.has(el) ? count++ : null));
    ///вариант с вложенным циклом
    /* for (const num1 of setArr1) {
      for (const num2 of setArr2) {
        if (num1 === num2) {
          count++;
        }
      }
    } */
    return count;
  }
  getSameCount([1, 3, 2, 2], [3, 1, 1, 2, 5]);
}
////////////////////////////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, 
которая получает на вход строку и считает, 
сколько символов (без учёта повторяющихся символов) использовано в этой строке.
Например, в строке yy используется всего один символ — y. А в строке 111yya! — используется четыре символа: 1, y, a и !. */

  ///// use new Set
  /*  function countUniqChars(string) {
    const set = new Set(string);
    return set.size;
  } */

  function countUniqChars(string) {
    return new Set(string.split('')).size;
  }
  // countUniqChars('You know nothing Jon Snow');
}
////////////////////////////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, 
  которая сортирует массив используя пузырьковую сортировку.  */
  function bubbleSort(arr) {
    let flag;
    do {
      flag = false;
      for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i] > arr[i + 1]) {
          let t = arr[i];
          arr[i] = arr[i + 1];
          arr[i + 1] = t;
          flag = true;
        }
      }
    } while (flag);
    return arr;
  }

  bubbleSort([3, 10, 4, 3]);
}
