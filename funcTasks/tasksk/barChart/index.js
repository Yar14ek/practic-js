'use strict';
// https://ru.hexlet.io/challenges/js_functions_bar_chart/instance
/* Реализуйте и экспортируйте по умолчанию функцию, которая выводит на экран столбчатую диаграмму.
Функция принимает в качестве параметра последовательность чисел, длина которой равна количеству столбцов диаграммы.
Размер диаграммы по вертикали должен определяться входными данными. */
const getMinMaxNum = ([first, second, ...rest]) => {
  let min, max;
  if (first > second) {
    max = first;
    min = second;
  } else {
    max = second;
    min = first;
  }

  for (const num of rest) {
    if (num > max || num < min) {
      if (num > max) max = num;
      else min = num;
    }
  }
  return [min, max];
};

const barChart = (numbers) => {
  const res = [];
  const [min, max] = getMinMaxNum(numbers);
  const uppBound = max < 0 ? 0 : max;
  const lowBound = min > 0 ? 0 : min;
  for (let i = uppBound; i >= lowBound; i -= 1) {
    if (i === 0) continue;
    let row = '';
    numbers.forEach((num) => {
      if (i > 0 && num >= i) {
        row += '*';
      } else if (i < 0 && num <= i) {
        row += '#';
      } else {
        row += ' ';
      }
    });
    res.push(row);
  }
  console.log(res.join('\n'));
};

barChart([5, 10, -5, -3, 7]);
// =>  *
//     *
//     *
//     *  *
//     *  *
//    **  *
//    **  *
//    **  *
//    **  *
//    **  *
//      ##
//      ##
//      ##
//      #
//      #

// barChart([5, -2, 10, 6, 1, 2, 6, 4, 8, 1, -1, 7, 3, -5, 5]);
// =>   *
//      *
//      *     *
//      *     *  *
//      **  * *  *
//    * **  * *  *  *
//    * **  ***  *  *
//    * **  ***  ** *
//    * ** ****  ** *
//    * ******** ** *
//     #        #  #
//     #           #
//                 #
//                 #
//                 #
