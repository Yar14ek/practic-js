'use strict';
// https://ru.hexlet.io/challenges/js_functions_sea_battle/instance
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход поле боя в виде квадратного двумерного массива из нулей и единиц.
Ноль — пустая ячейка, единица — часть корабля. Функция должна вернуть количество кораблей на поле боя.
В отличии от классической игры "Морской бой", в данном варианте корабли могут изгибаться. */
const calcShipsCount = (area) => {
  let res = 0;
  const length = area.length;
  for (let row = 0; row < length; row += 1) {
    for (let col = 0; col < length; col += 1) {
      if (area[row][col]) {
        if (!area[row][col - 1] && !area[row - 1]?.[col]) res += 1;
        if (area[row][col - 1] && area[row - 1]?.[col]) res -= 1;
      }
    }
  }
  return res;
};
// calcShipsCount([]); // 0
// calcShipsCount([
//   [0, 1, 0, 0, 0, 0],
//   [0, 1, 0, 0, 0, 1],
//   [0, 0, 0, 1, 0, 0],
//   [0, 1, 1, 1, 0, 1],
//   [0, 0, 0, 0, 0, 1],
//   [1, 1, 0, 1, 0, 0],
// ]); // 6
