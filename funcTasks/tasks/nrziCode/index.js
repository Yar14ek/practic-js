'use strict';
// https://ru.hexlet.io/challenges/js_functions_nrzi/instance
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает строку в виде графического представления линейного сигнала и возвращает строку с бинарным кодом.
Внимательно изучите примеры. 
Подсказки
Символ | в строке указывает на переключение сигнала и означает, что уровень сигнала в следующем такте,
будет изменён на противоположный по сравнению с предыдущим.
*/
const nrzi = (signal) => {
  let result = '';
  let bit = 0;
  for (const frag of signal) {
    if (frag === '|') {
      bit = 1;
      continue;
    }
    result += bit;
    bit = 0;
  }
  return result;
};

// const signal1 = '_|¯|____|¯|__|¯¯¯';
// nrzi(signal1); // '011000110100'

// const signal2 = '|¯|___|¯¯¯¯¯|___|¯|_|¯';
// nrzi(signal2); // '110010000100111'

// const signal3 = '¯|___|¯¯¯¯¯|___|¯|_|¯';
// nrzi(signal3); // '010010000100111'

// const signal4 = '';
// nrzi(signal4); // ''

// const signal5 = '|';
// nrzi(signal5); // ''
