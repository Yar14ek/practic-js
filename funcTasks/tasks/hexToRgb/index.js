// 'use strict';
// https://ru.hexlet.io/challenges/js_functions_rgb_hex_conversion/instance
/* Для задания цветов в HTML и CSS используются числа в шестнадцатеричной системе счисления.
Чтобы не возникало путаницы в определении системы счисления,
перед шестнадцатеричным числом ставят символ решетки #, например, #135278.
Обозначение цвета (rrggbb) разбивается на три составляющие,
где первые два символа обозначают красную компоненту цвета, два средних — зеленую,
а два последних — синюю.
Таким образом каждый из трех цветов — красный,
зеленый и синий — может принимать значения от 00 до FF в шестнадцатеричной системе исчисления.

solution.js
При работе с цветами часто нужно получить отдельные значения красного,
зеленого и синего (RGB) компонентов цвета в десятичной системе исчисления и наоборот.
Реализуйте и экспортируйте функции rgbToHex() и hexToRgb(), которые возвращают соответствующие представление цвета. */
const getPartOfColor = (color) => {
  const chankRgb = [[], [], []];
  let indexOfChank = 0;
  color
    .slice(1)
    .split('')
    .forEach((el, i) => {
      chankRgb[indexOfChank].push(el);
      if (i % 2) {
        indexOfChank += 1;
      }
    });
  return chankRgb;
};

const toDecimal = (part) => {
  const char16 = {
    a: 10,
    b: 11,
    c: 12,
    d: 13,
    e: 14,
    f: 15,
  };
  let deg = 1;
  return part.reduce((acc, el) => {
    acc += (isNaN(el) ? char16[el] : el) * 16 ** deg;
    deg -= 1;
    return acc;
  }, 0);
};

const hexToRgb = (color) => {
  const [r, g, b] = getPartOfColor(color);
  const result = {
    r: toDecimal(r),
    g: toDecimal(g),
    b: toDecimal(b),
  };
  return result;
};
const toHexadecimal = (num) => {
  const char10 = {
    10: 'a',
    11: 'b',
    12: 'c',
    13: 'd',
    14: 'e',
    15: 'f',
  };
  const calc = (el) => (el < 9 ? el : char10[el]);
  const int = calc(Math.floor(num / 16));
  const remain = calc(num % 16);
  return `${int}${remain}`;
};
const rgbToHex = (...args) => {
  return args.reduce((acc, el) => {
    acc += toHexadecimal(el);
    return acc;
  }, '#');
};

// version vith use toString() and parseIn()
const hexToRgb2 = (color) => {
  const shortNameCol = 'rgb';
  const colorPart = getPartOfColor(color);
  return colorPart.reduce((acc, el, i) => {
    acc[shortNameCol[i]] = parseInt(el.join(''), 16);
    return acc;
  }, {});
};

const rgbToHex2 = (...args) =>
  args.reduce((acc, num) => (acc += num.toString(16).padStart(2, '0')), '#');

// hexToRgb('#24ab00'); // { r: 36, g: 171, b: 0 }
// rgbToHex(36, 171, 0); // '#24ab00'
