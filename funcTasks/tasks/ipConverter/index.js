'use strict';
// https://ru.hexlet.io/challenges/js_functions_ip_converter/instance
/* Реализуйте и экспортируйте функции ipToInt() и intToIp(),
которые преобразовывают представление IP-адреса из десятичного формата с точками в 32-битное число в десятичной форме и обратно.

Функция ipToInt() принимает на вход строку и должна возвращать число.
А функция intToIp() наоборот: принимает на вход число, а возвращает строку. */

const ipToInt = (ip) => {
  const partIp = ip.split('.');
  const res = partIp.reduce((acc, el, i) => {
    acc += el * Math.pow(256, 3 - i);
    return acc;
  }, 0);
  return res;
};

/* const intToIp = (num) => {
  const ipHex = num.toString(16).padStart(8, 0);
  const res = [];
  const { length } = ipHex;
  for (let i = 0; i < length; i += 2) {
    const part = ipHex.slice(i, i + 2);
    res.push(parseInt(part, 16));
  }
  return res.join('.');
}; */

const intToIp = (int) => {
  let num = int;
  const res = [];
  for (let i = 3; i >= 0; i -= 1) {
    const remain = num % 256 ** i;
    const quarter = Math.floor(num / 256 ** i);
    res.push(quarter);
    num = remain;
  }
  return res.join('.');
};

// ipToInt('128.32.10.1'); // 2149583361
// ipToInt('0.0.0.0'); // 0
// ipToInt('255.255.255.255'); // 4294967295

// intToIp(2149583361); // '128.32.10.1'
// intToIp(0); // '0.0.0.0'
// intToIp(4294967295); // '255.255.255.255'
