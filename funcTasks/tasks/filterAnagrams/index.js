'use strict';
// https://ru.hexlet.io/challenges/js_functions_filter_anagrams/instance
/* Анаграммы — это слова, которые состоят из одинаковых букв. Например:

спаниель — апельсин
карат — карта — катар
топор — ропот — отпор
filterAnagrams.js
Реализуйте и экспортируйте по умолчанию функцию, которая находит все анаграммы слова.
Функция принимает исходное слово и список для проверки (массив), а возвращает массив всех анаграмм.
Если в списке слов отсутствуют анаграммы, то возвращается пустой массив. */
const formatString = (str) => {
  return str.split('').sort().join('');
};
const filterAnagrams = (word, words) => {
  const formatWord = formatString(word);
  return words.filter((el) => formatString(el) === formatWord);
};
// filterAnagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']);
// ['aabb', 'bbaa']

// filterAnagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']);
// ['carer', 'racer']

// filterAnagrams('laser', ['lazing', 'lazy', 'lacer']);
// []
