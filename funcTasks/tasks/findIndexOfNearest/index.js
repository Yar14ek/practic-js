'use strict';
// https://ru.hexlet.io/challenges/js_functions_find_nearest/instance
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход массив чисел и искомое число.
Задача функции — найти в массиве ближайшее число к искомому и вернуть его индекс в массиве.

Если в массиве содержится несколько чисел,
одновременно являющихся ближайшими к искомому числу, то возвращается наименьший из индексов ближайших чисел. */
const findIndexOfNearest = (nums, searchNum) => {
  const result = nums.reduce((acc, num, i) => {
    const diff = Math.abs(searchNum - num);
    if (!acc.hasOwnProperty('differ')) {
      acc.differ = diff;
      acc.index = i;
    } else {
      if (acc.differ > diff) {
        acc.differ = diff;
        acc.index = i;
      }
    }
    return acc;
  }, {});
  return nums.length ? result.index : null;
};
// findIndexOfNearest([], 2); // null
// findIndexOfNearest([15, 10, 3, 4], 0); // 2
// findIndexOfNearest([7, 5, 3, 2], 4); // 1
// findIndexOfNearest([7, 5, 4, 4, 3], 4); // 2
