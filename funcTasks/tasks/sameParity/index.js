'use strict';
// https://ru.hexlet.io/challenges/js_functions_same_parity/instance
/* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход массив и возвращает новый,
состоящий из элементов, у которых такая же чётность, как и у первого элемента входного массива. */

const sameParity = (numbers) => {
  const standart = Math.abs(numbers[0] % 2);
  return numbers.filter((num) => Math.abs(num % 2) === standart);
};
// sameParity([-1, 0, 1, -3, 10, -2]); // [-1, 1, -3]
// sameParity([2, 0, 1, -3, 10, -2]); // [2, 0, 10, -2]
// sameParity([]); // []
