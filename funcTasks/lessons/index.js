'use strict';
// https://ru.hexlet.io/courses/js-functions/lessons/pure-functions/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию, которая проверяет переданное число на простоту и печатает на экран yes или no.
Примеры
sayPrimeOrNot(5); // 'yes'
sayPrimeOrNot(4); // 'no'
Подсказки
Цель этой задачи — научиться отделять чистый код от кода с побочными эффектами.

Для этого выделите процесс определения того, является ли число простым, в отдельную функцию, возвращающую логическое значение.
Это функция, с помощью которой мы отделяем чистый код от кода,
интерпретирующего логическое значение (как 'yes' или 'no') и делающего побочный эффект (печать на экран).*/
{
  const isPrime = (num) => {
    let result = num > 2;
    for (let i = 2; i < num; i += 1) {
      if (!(num % i)) {
        result = false;
        break;
      }
    }
    return result;
  };

  const sayPrimeOrNot = (num) => {
    const result = isPrime(num) ? 'yes' : 'no';
    console.log(result);
  };
  //   sayPrimeOrNot(1);
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/rest-operator/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию, которая возвращает среднее арифметическое всех переданных аргументов.
Если функции не передать ни одного аргумента, то она должна вернуть null.

Примеры
average(0); // 0
average(0, 10); // 5
average(); // null */
{
  const calc = (array) =>
    array.reduce((count, num) => (count += num)) / array.length;

  const average = (...nums) => {
    return nums.length ? calc(nums) : null;
  };
  //   average(-3, 4, 2, 10); // 3.25
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/spread-operator/exercise_unit
/* Реализуйте функцию, которая конвертирует даты в массив человеко-читаемых строк на английском языке.
Каждая из дат представлена массивом [2001, 10, 18], в котором первый элемент — это год, второй — месяц, и третий — число.
Функция на вход должна принимать любое количество параметров. Если в функцию ничего не было передано, она должна вернуть пустой массив.
Экспортируйте функцию по умолчанию.

Примеры:
convert();
// []

convert([1993, 3, 24]);
// ['Sat Apr 24 1993']

*/

const convert = (...dates) => {
  return dates.map((el) => {
    return new Date(...el).toDateString();
  });
};
// convert([1993, 3, 24], [1997, 8, 12], [2001, 10, 18]);
// ['Sat Apr 24 1993', 'Fri Sep 12 1997', 'Sun Nov 18 2001']
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/first-class-citizen/exercise_unit
/* Реализуйте внутреннюю функцию takeLast(), которая возвращает последние n символов строки в обратном порядке.
Количество символов передаётся в takeLast() вторым параметром.
Если передаётся пустая строка или строка меньше необходимой длины, функция должна вернуть null. */
{
  /*  const takeLast = (str, leng) => {
    if (str.length < leng) return null;
    const endStr = str.slice(-leng);
    return endStr.split('').reverse().join('');
  }; */
  const takeLast = (str, cutLength) => {
    let res = null;
    const length = str.length;
    if (length >= cutLength) {
      res = '';
      for (let i = length - 1; i >= length - cutLength; i--) {
        res += str[i];
      }
    }
    return res;
  };
  const run = (text) => {
    return takeLast(text, 4);
  };
  // run(''); // null
  // run('cb'); // null
  // run('power'); // rewo
  // run('hexlet'); // telx
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/high-order-functions/exercise_unit
/* Реализуйте функцию takeOldest(), которая принимает на вход список пользователей и возвращает самых взрослых.
Количество возвращаемых пользователей задается вторым параметром, который по умолчанию равен единице.
Экспортируйте данную функцию по умолчанию.

Пример использования

Другие примеры смотрите в модуле с тестами.

Подсказки
Для преобразования дат в единое представление — unixtimestamp — используйте метод Date.parse()
В рамках данного упражнения, для записи дат используется только формат RFC2822.
sortBy
Подумайте, что из себя представляет данная функция: команду или запрос? */
{
  const takeOldest = (users, count = 1) => {
    const usersSort = users.sort(
      (a, b) => Date.parse(a.birthday) - Date.parse(b.birthday)
    );
    return usersSort.slice(0, count);
  };

  const users = [
    { name: 'Tirion', birthday: 'Nov 19, 1988' },
    { name: 'Sam', birthday: 'Nov 22, 1999' },
    { name: 'Rob', birthday: 'Jan 11, 1975' },
    { name: 'Sansa', birthday: 'Mar 20, 2001' },
    { name: 'Tisha', birthday: 'Feb 27, 1992' },
    { name: 'Chris', birthday: 'Dec 25, 1995' },
  ];

  //   takeOldest(users);
  // [
  //   { name: 'Rob', birthday: 'Jan 11, 1975' },
  // ];
}

///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/map/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список пользователей и возвращает плоский список их детей.
Дети каждого пользователя хранятся в виде массива в ключе children.
 */

{
  const getChildren = (users) =>
    users.reduce((acc, { children }) => {
      acc = [...acc, ...children];
      return acc;
    }, []);

  const users = [
    {
      name: 'Tirion',
      children: [{ name: 'Mira', birthday: '1983-03-23' }],
    },
    { name: 'Bronn', children: [] },
    {
      name: 'Sam',
      children: [
        { name: 'Aria', birthday: '2012-11-03' },
        { name: 'Keit', birthday: '1933-05-14' },
      ],
    },
    {
      name: 'Rob',
      children: [{ name: 'Tisha', birthday: '2012-11-03' }],
    },
  ];
  // getChildren(users);
  // [
  //   { name: 'Mira', birthday: '1983-03-23' },
  //   { name: 'Aria', birthday: '2012-11-03' },
  //   { name: 'Keit', birthday: '1933-05-14' },
  //   { name: 'Tisha', birthday: '2012-11-03' },
  // ];
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/filter/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход список пользователей и возвращает плоский список подруг всех пользователей (без сохранения ключей).
Друзья каждого пользователя хранятся в виде массива в ключе friends.
Пол доступен по ключу gender и может принимать значения male или female. */

{
  const getGirlFriends = (users) => {
    return users
      .map(({ friends }) =>
        friends.filter((friend) => friend.gender === 'female')
      )
      .flat();
  };

  const users = [
    {
      name: 'Tirion',
      friends: [
        { name: 'Mira', gender: 'female' },
        { name: 'Ramsey', gender: 'male' },
      ],
    },
    { name: 'Bronn', friends: [] },
    {
      name: 'Sam',
      friends: [
        { name: 'Aria', gender: 'female' },
        { name: 'Keit', gender: 'female' },
      ],
    },
    {
      name: 'Rob',
      friends: [{ name: 'Taywin', gender: 'male' }],
    },
  ];

  // getGirlFriends(users);
  // [
  //   { name: 'Mira', gender: 'female' },
  //   { name: 'Aria', gender: 'female' },
  //   { name: 'Keit', gender: 'female' },
  // ];
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/reduce/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию для группировки объектов по заданному свойству.
Функция принимает аргументами массив объектов и название свойству для группировки.
Она должна возвращать объект, где ключ - это значение по заданному свойству, а значение - массив с данными, подходящими для группы. */
{
  const groupBy = (array, props) => {
    return array.reduce((acc, el) => {
      acc[el[props]] = acc[el[props]] ?? [];
      acc[el[props]].push(el);
      return acc;
    }, {});
  };
  const students = [
    { name: 'Tirion', class: 'B', mark: 3 },
    { name: 'Keit', class: 'A', mark: 3 },
    { name: 'Ramsey', class: 'A', mark: 4 },
  ];

  // groupBy([], ''); // {}
  // groupBy(students, 'mark');
  // {
  //   3: [
  //     { name: "Tirion", class: "B", mark: 3 },
  //     { name: "Keit", class: "A", mark: 3 },
  //   ],
  //   4: [
  //     { name: "Ramsey", class: "A", mark: 4 },
  //   ],
  // }
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/signals/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список емейлов,
а возвращает количество емейлов, расположенных на каждом бесплатном домене.
Список бесплатных доменов хранится в константе freeEmailDomains. */
{
  const freeEmailDomains = ['gmail.com', 'yandex.ru', 'hotmail.com'];
  const getFreeDomainsCount = (emails) => {
    const freeDomainsSet = new Set(freeEmailDomains);
    return emails.reduce((acc, email) => {
      const [, domain] = email.split('@');
      if (freeDomainsSet.has(domain)) {
        acc[domain] = (acc[domain] || 0) + 1;
      }
      return acc;
    }, {});
  };
  const emails = [
    'info@gmail.com',
    'info@yandex.ru',
    'info@hotmail.com',
    'mk@host.com',
    'support@hexlet.io',
    'key@yandex.ru',
    'sergey@gmail.com',
    'vovan@gmail.com',
    'vovan@hotmail.com',
  ];

  // getFreeDomainsCount(emails);
  // {
  //   'gmail.com': 3,
  //   'yandex.ru': 2,
  //   'hotmail.com': 2,
  // };
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// https://ru.hexlet.io/courses/js-functions/lessons/paradigms/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает изображение в виде двумерного массива и увеличивает его в два раза. */
{
  const doubleSize = (arr) => {
    const result = [...arr];
    const length = result.length;
    for (let i = 0; i < length; i++) {
      result.splice(i * 2, 0, arr[i]);
    }
    return result;
  };

  const enlargeArrayImage = (arr) => {
    const widthSize = arr.map((row) => doubleSize(row));
    return doubleSize(widthSize);
  };

  const arr = [
    ['*', '*', '*', '*'],
    ['*', ' ', ' ', '*'],
    ['*', ' ', ' ', '*'],
    ['*', '*', '*', '*'],
  ];
  // ****
  // *  *
  // *  *
  // ****
  // enlargeArrayImage(arr);
  // ********
  // ********
  // **    **
  // **    **
  // **    **
  // **    **
  // ********
  // ********
}
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
