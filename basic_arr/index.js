'use strict';
{
  // if arg = 'long' return full name if 'short' return short name, else full name
  function getWeekends(arg) {
    const long = ['friday', 'saturday'];
    const short = ['fr', 'sat'];
    switch (arg) {
      case 'long':
        return long;
      case 'short':
        return short;
      default:
        return long;
    }
  }
}

{
  //remove ferst and last item in arr
  function swap(arr) {
    if (arr.length < 2) {
      return arr;
    }
    const item = arr[0];
    arr[0] = arr[arr.length - 1];
    arr[arr.length - 1] = item;
    return arr;
  }
}

{
  //if elem exist return this elem, else return data
  const cities = ['moscow', 'london', 'berlin', 'porto', ''];

  function get(arr, i, data = null) {
    return arr[i] !== undefined ? arr[i] : data;
  }
  // get(cities, 10, 'default');
  // get(cities, 2,);
}

{
  //create new arr. Цhich each element add prefix
  function addPrefix(arr, pref) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
      newArr.push(`${pref} ${arr[i]}`);
    }
    return newArr;
  }
}

{
  // create function reverse
  function reverse(arr) {
    let num = Math.floor(arr.length / 2);
    let index = arr.length - 1;
    let elem = '';

    for (let i = 0; i < num; i++) {
      elem = arr[i];
      arr[i] = arr[index - i];
      arr[index - i] = elem;
    }
    return arr;
  }
  // reverse(['john', 'smith', 'karl'])
  // reverse(['john', 'smith', 'karl', 'alan', 'joe'])
}

{
  //use reduce
  function calculateSum(arr) {
    return arr.reduce((summ, el) => (el % 3 < 1 ? summ + el : summ), 0);
  }

  //use for statement
  /* function calculateSum(arr) {
    let summ = 0;0
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] % 3 < 1) {
        summ += arr[i];
      }
    }
    return summ;
  } */
  // calculateSum([8, 9, 21, 19, 18, 22, 7]);
}

{
  //find average. If arr empti return null. use for...of
  function calculateAverage(arr) {
    let summ = 0;
    for (const t of arr) {
      summ += t;
    }
    return arr.length ? summ / arr.length : null;
  }
  //   calculateAverage([37.5, 34, 39.3, 40, 38.7, 41.5]);
}

{
  /* Реализуйте функцию, которая принимает на вход массив чисел и возвращает новый,
     состоящий из элементов,
      у которых такая же чётность, как и у первого элемента входного массива. Бывают и отрицательные числа */
  function getSameParity(arr) {
    const newArr = [];
    const flag = Math.abs(arr[0] % 2);
    for (const num of arr) {
      if (num < 0 && Math.abs(num % 2) === flag) {
        newArr.push(num);
      }
      if (num % 2 === flag) {
        newArr.push(num);
      }
    }
    return newArr;
  }
  console.log(getSameParity([1, 2, -3]));
  //   getSameParity([-3, 1]);
}

{
  /* Реализуйте функцию, которая принимает на вход в виде массива кошелёк с деньгами и название валюты
и возвращает сумму денег указанной валюты.
Реализуйте данную функцию используя управляющие инструкции. */
  function getTotalAmount(arr, cur) {
    let summ = 0;
    for (let str of arr) {
      if (str.slice(0, 3) !== cur) {
        continue;
      }
      summ += +str.slice(4);
    }
    return summ;
  }
  //   getTotalAmount(['eur 10', 'usd 1', 'usd 10', 'rub 50', 'usd 5'], 'usd');
}

{
  /* Реализуйте функцию, которая находит команду победителя для конкретной суперсерии.
 Победитель определяется как команда, у которой больше побед (не количество забитых шайб) в конкретной серии. 
 Функция принимает на вход массив, 
 в котором каждый элемент — это массив описывающий счет в конкретной игре (сколько шайб забила Канада и СССР).
Результат функции – название страны: 'canada', 'ussr'. Если суперсерия закончилась в ничью, то нужно вернуть null. */
  function getSuperSeriesWinner(arr) {
    let canada = 0;
    let ussr = 0;
    for (const game of arr) {
      if (game[0] < game[1]) {
        ussr++;
      } else if (game[0] > game[1]) {
        canada++;
      }
    }
    return canada === ussr ? null : canada > ussr ? 'canada' : 'ussr';
  }
  //   getSuperSeriesWinner([
  //     [3, 7],
  //     [4, 4],
  //     [3, 5],
  //     [4, 5],
  //     [3, 2],
  //     [4, 3],
  //     [6, 5],
  //   ]);
}
