'use strict';
import crc32 from 'crc-32';

{
  /* Реализуйте и экспортируйте по умолчанию функцию, которая "нормализует" данные переданного урока.
То есть приводит их к определенному виду.

На вход этой функции подается объект описывающий собой урок курса. В уроке содержатся два поля: имя и описание.

const lesson = {
  name: 'Деструктуризация',
  description: 'как удивить колек',
};
У некоторых уроков имя и описание могут быть в разном регистре. Такое случается при вводе данных:

const lesson = {
  name: 'ДеструКТУРИЗАЦИЯ',
  description: 'каК удивитЬ колек',
};
Наша функция должна обновлять содержимое урока по следующим правилам:

Имя капитализируется. То есть первый символ имени становится заглавным, остальные маленькими.
Весь текст описания приводится к нижнему регистру. */
  let a = { name: 'intro', description: 'about Something' };
  function normalize(obj) {
    const firstChar = obj.name.charAt(0).toUpperCase(); //если передать пустую строку в name, то при записи obj.name[0].toUpperCase() выдает ошибку
    obj.name = `${firstChar}${obj.name.substring(1)}`;
    obj.description = obj.description.toLowerCase();
  }
  //   normalize(a);
  //   console.log('a :>> ', a);
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, которая сравнивает объекты по совпадению данных, а не по ссылкам.
    Эта функция принимает на вход две компании и возвращает true если их структура одинаковая и false в обратном случае.
    Проверка должна проходить по свойствам name, state, website. */
  function is(obj1, obj2) {
    const props = ['name', 'state', 'website'];
    for (const prop of props) {
      if (obj1[prop] !== obj2[prop]) {
        return false;
      }
    }
    return true;
  }
  const company1 = {
    name: 'Hexlet',
    state: 'published',
    website: 'https://hexlet.io',
    country: 'Ukraine',
  };
  const company2 = {
    name: 'Hexlet',
    state: 'published',
    website: 'https://hexlet.io',
    cost: 1000,
  };
  // is(company1, company2);
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию,
 которая принимает на вход имя сайта и возвращает информацию о нем:
// Если домен передан без указания протокола,
// то по умолчанию берется http
getDomainInfo('yandex.ru')
// {
//   scheme: 'http',
//   name: 'yandex.ru',
// }

getDomainInfo('https://hexlet.io');
// {
//   scheme: 'https',
//   name: 'hexlet.io',
// }

getDomainInfo('http://google.com');
// {
//   scheme: 'http',
//   name: 'google.com',
// } */

  function getDomainInfo(string) {
    const obj = {
      scheme: 'http',
      name: string,
    };
    const splitUrl = string.split('//');
    if (splitUrl.length > 1) {
      obj.scheme = splitUrl[0].slice(0, -1);
      obj.name = splitUrl[1];
    }
    return obj;
  }
  // getDomainInfo('yandex.ru');
  // getDomainInfo('http://google.com');
  // getDomainInfo('ftp://google.com');
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, 
которая считает количество слов в предложении и возвращает объект в котором свойства это слова (приведенные к нижнему регистру),
а значения — это то сколько раз слово встретилось в предложении. 
Слова в предложении могут находиться в разных регистрах. 
Перед подсчетом их нужно приводить в нижний регистр, чтобы не пропускались дубли.*/

  function countWords(string) {
    if (!string.length) {
      return {};
    }
    const wordsArr = string.split(' ').map((el) => el.toLowerCase());
    const res = {};
    wordsArr.forEach((el) => {
      res[el] = (res[el] || 0) + 1; //работает!
      // if (res.hasOwnProperty(el)) {
      //   res[el] += 1;
      // } else {
      //   res[el] = 1;
      // }
    });
    return res;
  }
  const text2 = 'Another One sentence with strange Words words';
  // countWords(text2);
  //   countWords('');
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте функцию по умолчанию, 
  которая формирует из переданного объекта другой объект,
  включающий только указанные свойства. Параметры:
  
  Исходный объект
  Массив имен свойств */

  function pick(obj, arr) {
    const props = new Set(arr);
    const res = {};
    for (const key in obj) {
      if (props.has(key)) {
        res[key] = obj[key];
      }
    }
    return res;
  }
  // const data = {
  //   user: 'ubuntu',
  //   cores: 4,
  //   os: 'linux',
  // };
  // const user = {
  //   name: 'Yaroslav',
  //   __proto__: data,
  // };
  // pick(user, ['user', 'os', 'name']);
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию,
  которая извлекает из объекта любой глубины вложенности значение по указанным ключам. Параметры:
  Исходный объект
  Цепочка ключей (массив), по которой ведётся поиск значения
  В случае, когда добраться до значения невозможно, возвращается null. */

  //  в дебаг консоли выскакивают ошибки при использовании операторов '?.' и '??'
  function get(data, arr) {
    let res = data;
    for (let i = 0; i < arr.length && res !== undefined; i++) {
      res = res?.[arr[i]];
    }
    return res ?? null;
  }

  const data = {
    user: 'ubuntu',
    hosts: {
      0: {
        name: 'web1',
      },
      1: {
        name: 'web2',
        null: 3,
        active: false,
      },
    },
  };
  // console.log(get(data, ['undefined'])); // null
  // console.log(get(data, ['user'])); // 'ubuntu'
  // console.log(get(data, ['user', 'ubuntu'])); // null
  // console.log(get(data, ['hosts', 1, 'name'])); // 'web2'
  // console.log(get(data, ['hosts', 0])); // { name: 'web1' }
  // console.log(get(data, ['hosts', 1, null])); // 3
  // console.log(get(data, ['hosts', 1, 'active'])); // false
}
/////////////////////////////////////////////////////////////////////////
{
  /*  Реализуйте и экспортируйте по умолчанию функцию,
  которая заполняет объект данными из другого объекта по разрешенному списку ключей.
  Параметры:
  
  Исходный объект
  Список ключей которые нужно заменить
  Данные, которые нужно сливать в исходный объект
  В случае, когда список ключей пустой, нужно сливать все данные полностью. */
  function fill(soursObj, propToChange, data) {
    if (!propToChange.length) {
      Object.assign(soursObj, data);
    } else {
      propToChange.forEach((prop) => (soursObj[prop] = data[prop]));
    }
  }
  const company = {
    name: null,
    state: 'moderating',
  };

  const data = {
    name: 'Hexlet',
    state: 'published',
  };
  // fill(company, ['name'], data);
  // fill(company, [], data);
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, которая выполняет глубокое копирование объектов.  */
  function cloneDeep(obj) {
    const res = {};
    for (const prop in obj) {
      if (typeof obj[prop] === 'object') {
        res[prop] = cloneDeep(obj[prop]);
      } else {
        res[prop] = obj[prop];
      }
    }
    return res;
  }

  const data = {
    key: 'value',
    key2: {
      key: 'innerValue',
      innerKey: {
        anotherKey: 'anotherValue',
      },
    },
  };
  // const res = cloneDeep(data);
  // res.key2.key = 'Some change';
  // console.log(res.key2 !== data.key2);
  // console.log('data=====>', JSON.stringify(data, {}, ' '));
  // console.log('res====>', JSON.stringify(res, {}, ' '));
}
/////////////////////////////////////////////////////////////////////////
{
  /*   Реализуйте и экспортируйте по умолчанию функцию, которая создает объект компании и возвращает его.
  Для создания компании обязательно только одно свойство – имя компании. 
  Остальные свойства добавляются только если они есть. Параметры:
  
  Имя
  Объект с дополнительными свойствами
  Также у компаний есть два свойства со значениями по умолчанию:
  
  state – moderating
  createdAt – текущая дата */
  function make(name, obj) {
    return {
      name,
      state: 'moderating',
      createdAt: Date.now(),
      ...obj,
    };
  }
  // console.log(make('Hexlet', { website: 'hexlet.io', state: 'published' }));
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход список пользователей, 
  извлекает их имена, сортирует в алфавитном порядке и возвращает отсортированный список имен. */
  function getSortedNames(users) {
    return users.map(({ name }) => name).sort();
  }

  const users = [
    { name: 'Bronn', gender: 'male', birthday: '1973-03-23' },
    { name: 'Reigar', gender: 'male', birthday: '1973-11-03' },
    { name: 'Eiegon', gender: 'male', birthday: '1963-11-03' },
    { name: 'Sansa', gender: 'female', birthday: '2012-11-03' },
  ];
  // getSortedNames(users);
}
/////////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте набор функций, для работы со словарём, построенным на хеш-таблице. Для простоты, наша реализация не поддерживает разрешение коллизий.

По сути в этом задании надо реализовать объекты. По понятным причинам использовать объекты для создания объектов нельзя. Представьте что в языке объектов нет и мы их хотим добавить.

make() — создаёт новый словарь
set(map, key, value) — устанавливает в словарь значение по ключу. Работает и для создания и для изменения. Функция возвращает true, если удалось установить значение. При возникновении коллизии, функция никак не меняет словарь и возвращает false.
get(map, key, defaultValue = null) — возвращает значение указанного ключа. Параметр defaultValue — значение, которое функция возвращает, если в словаре нет ключа (по умолчанию равно null). При возникновении коллизии функция также возвращает значение по умолчанию.
Функции set и get принимают первым параметром словарь. Передача идёт по ссылке, поэтому set может изменить его напрямую. */
  const make = () => [];

  const createIndex = (key) => {
    const hash = crc32.str(key);
    return Math.floor(Math.abs(hash / 1000));
  };

  const set = (map, key, value) => {
    let res = true;
    const index = createIndex(key);
    if (!map[index]) {
      map[index] = [key, value];
    } else if (map[index][0] === key) {
      map[index][1] = value;
    } else {
      res = false;
    }
    return res;
  };

  const get = (map, key, value = null) => {
    const index = createIndex(key);
    if (map[index] && map[index][0] === key) value = map[index][1];
    return value;
  };

  const map = make();
  get(map, 'key'); //null
  get(map, 'key', 'value'); //value
  set(map, 'key2', 'value2');
  get(map, 'key2'); //value2
  get(map, 'undefined'); //null
  set(map, 'key2', 'another value');
  get(map, 'key2'); //another value
  set(map, 'aaaaa0.462031558722291', 'vvv');
  set(map, 'aaaaa0.0585754039730588', 'boom!');
  get(map, 'aaaaa0.462031558722291'); //vvv
  get(map, 'aaaaa0.0585754039730588'); //null
  set(map, 'aaaaa0.462031558722291', 'wop');
  get(map, 'aaaaa0.462031558722291'); //wop
}
