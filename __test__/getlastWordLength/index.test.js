import getLastWordLength from '../../arrays_tasks/lastWordLength/index';

test('lastWordLength', () => {
  expect(getLastWordLength('hello, wOrLD!  ')).toEqual(6);
  expect(getLastWordLength('BlaCk')).toEqual(5);
});
