import flatten from '../../../trees/flatten/';

test('should return flat array', () => {
  expect(flatten([1, 2, [3, 5], [[4, 3], 2]])).toEqual([1, 2, 3, 5, 4, 3, 2]);
});
