import convert from '../../../trees/convert/';

test('should convert arrays to object', () => {
  expect(convert([['key', 'value']])).toEqual({ key: 'value' });
  expect(
    convert([
      ['key', [['key2', 'anotherValue']]],
      ['key2', 'value2'],
    ])
  ).toEqual({ key: { key2: 'anotherValue' }, key2: 'value2' });
});
