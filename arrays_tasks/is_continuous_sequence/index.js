'use strict';
// https://ru.hexlet.io/challenges/js_arrays_ascending_sequence/instance
/* Реализуйте и экспортируйте по умолчанию функцию, которая проверяет,
является ли переданная последовательность целых чисел возрастающей непрерывно (не имеющей пропусков чисел).
Например, последовательность [4, 5, 6, 7] — непрерывная, а [0, 1, 3] — нет.
Последовательность может начинаться с любого числа, главное условие — отсутствие пропусков чисел.
Последовательность из одного числа не может считаться возрастающей.

Примеры
isContinuousSequence([-5, -4, -3]);         // true

isContinuousSequence([1, 2, 2, 3]);         // false
isContinuousSequence([7]);                  // false
isContinuousSequence([]);                   // false 
*/
function isContinuousSequence(sequence) {
  let result = sequence.length > 1 ? true : false;
  for (let i = 0; i < sequence.length - 1; i++) {
    if (sequence[i] + 1 !== sequence[i + 1]) {
      result = false;
      break;
    }
  }
  return result;
}
// isContinuousSequence([10, 11, 12, 14, 15]); // false
// isContinuousSequence([10, 11, 12, 13]); // true
