'use strict';
// https://ru.hexlet.io/challenges/js_arrays_chunk/instance
/* Реализуйте и экспортируйте функцию по умолчанию,
которая принимает на вход массив и число, которое задает размер чанка (куска).
Функция должна вернуть массив, состоящий из чанков указанной размерности.
*/

const chunk = (array, chunkSize) => {
  const numberOfChunk = Math.ceil(array.length / chunkSize);
  const chunks = Array(numberOfChunk)
    .fill()
    .map(() => Array());
  let chunkIndex = 0;
  for (let i = 0; i < array.length; i++) {
    chunks[chunkIndex].push(array[i]);
    if (!((i + 1) % chunkSize)) chunkIndex++;
  }
  return chunks;
};

chunk(['a', 'b', 'c', 'd'], 2);
// [['a', 'b'], ['c', 'd']]

// chunk(['a', 'b', 'c', 'd'], 3);
// [['a', 'b', 'c'], ['d']]
