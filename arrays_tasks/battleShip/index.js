'use strict';
// https://ru.hexlet.io/challenges/js_arrays_sea_battle/instance
/* Реализуйте и экспортируйте функцию calcShipsCount(),
которая принимает на вход поле боя в виде квадратного двумерного массива из нулей и единиц.
Ноль — пустая ячейка, единица — часть корабля. Функция должна вернуть количество кораблей на поле боя.

Так как корабли не должны соприкасаться друг с другом, реализуйте и экспортируйте функцию isValidField(),
которая проверяет расстановку кораблей на корректность.

Примеры
calcShipsCount([]); // 0


 */

const calcShipsCount = (matrix) => {
  if (!isValidField(matrix)) return 'matrix not valid';
  let count = 0;
  const length = matrix.length;
  for (let row = 0; row < length; row++) {
    for (let col = 0; col < length; col++) {
      if (matrix[row][col]) {
        if (
          (matrix[row - 1] && !matrix[row - 1][col] && !matrix[row][col - 1]) || // страшно выглядит??
          (!matrix[row - 1] && !matrix[row][col - 1])
        )
          count++;
      }
    }
  }
  return count;
};
const isValidField = (matrix) => {
  let result = true;
  const length = matrix.length;
  loop1: for (let row = 0; row < length; row++) {
    for (let col = 0; col < length - 1; col++) {
      if (matrix[row][col]) {
        if (
          (matrix[row - 1] && matrix[row - 1][col + 1]) ||
          (matrix[row + 1] && matrix[row + 1][col + 1])
        ) {
          result = false;
          break loop1;
        }
      }
    }
  }
  return result;
};
// calcShipsCount([
//   [0, 1, 0, 0, 0, 0],
//   [0, 1, 0, 1, 1, 1],
//   [0, 0, 0, 0, 0, 0],
//   [0, 1, 1, 1, 0, 1],
//   [0, 0, 0, 0, 0, 1],
//   [1, 1, 0, 1, 0, 0],
// ]); // 6

// isValidField([
//   [0, 1, 0, 0],
//   [1, 0, 0, 1],
//   [0, 0, 0, 0],
//   [0, 1, 1, 1],
// ]); // false
