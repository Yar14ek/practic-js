'use strict';
/* В данном упражнении необходимо реализовать стековую машину, то есть алгоритм, проводящий вычисления по обратной польской записи.
Обратная польская нотация или постфиксная нотация — форма записи математических и логических выражений
, в которой операнды расположены перед знаками операций. Выражение читается слева направо.
 Когда в выражении встречается знак операции, выполняется соответствующая операция над двумя ближайшими операндами,
  находящимися слева от знака операции. Результат операции заменяет в выражении последовательность её операндов и знак, 
  после чего выражение вычисляется дальше по тому же правилу. 
  Таким образом, результатом вычисления всего выражения становится результат последней вычисленной операции.
Например, выражение (1 + 2) * 4 + 3 в постфиксной нотации будет выглядеть так: 1 2 + 4 * 3 +, а результат вычисления: 15.
 Другой пример - выражение: 7 - 2 * 3, в постфиксной нотации: 7 2 3 * -, результат: 1.

solution.js
Реализуйте функцию calcInPolishNotation, которая принимает массив, каждый элемент которого содержит число или знак операции (+, -, *, /).
Функция должна вернуть результат вычисления по обратной польской записи. Экспортируйте функцию по умолчанию. */

function calcInPolishNotation(arr) {
  const operator = new Set(['+', '-', '*', '/']);
  let stack = [];
  for (let i = 0; i < arr.length; i++) {
    if (!operator.has(arr[i])) {
      stack.push(arr[i]);
      continue;
    }
    let operationRes = 0;
    let secondNum = stack.pop();
    let firstNum = stack.pop();
    switch (arr[i]) {
      case '*':
        operationRes = firstNum * secondNum;
        break;
      case '/':
        operationRes = firstNum / secondNum;
        break;
      case '+':
        operationRes = firstNum + secondNum;
        break;
      case '-':
        operationRes = firstNum - secondNum;
        break;
      default:
        break;
    }
    stack.push(operationRes);
  }
  return stack[0];
}
const polishNotashionArr = [7, 2, 3, '*', '-'];
// const polishNotashionArr = [1, 2, '+', 4, '*', 3, '/'];
calcInPolishNotation(polishNotashionArr); // 15
console.log('polishNotashionArr :>> ', polishNotashionArr);
