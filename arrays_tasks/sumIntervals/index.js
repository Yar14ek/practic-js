'use strict';
// https://ru.hexlet.io/challenges/js_arrays_sum_intervals/instance
/* Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход массив интервалов и возвращает сумму всех длин интервалов.
В данной задаче используются только интервалы целых чисел от -100 до 100 , которые представлены в виде массива.
Первое значение интервала всегда будет меньше, чем второе значение.
Например, длина интервала [-100, 0] равна 100, а длина интервала [5, 5] равна 0.
Пересекающиеся интервалы должны учитываться только один раз. */

const sumIntervals = (intervals) => {
  const numOfInterval = new Set();
  for (const [min, max] of intervals) {
    for (let i = min; i < max; i++) {
      if (!numOfInterval.has(i)) {
        numOfInterval.add(i);
      }
    }
  }
  return numOfInterval.size;
};
// sumIntervals([
//   [1, 2],
//   [11, 12],
// ]);
// sumIntervals([
//   [1, 5],
//   [-30, 19],
//   [1, 7],
//   [16, 19],
//   [5, 100],
// ]);
