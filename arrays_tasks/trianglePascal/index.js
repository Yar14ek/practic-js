'use strict';
// https://ru.hexlet.io/challenges/js_arrays_pascals_triangle/instance
/* Треугольник Паскаля — бесконечная таблица биномиальных коэффициентов, имеющая треугольную форму.
В этом треугольнике на вершине и по бокам стоят единицы. Каждое число равно сумме двух расположенных над ним чисел.
Строки треугольника симметричны относительно вертикальной оси.

0:      1
1:     1 1
2:    1 2 1
3:   1 3 3 1
4:  1 4 6 4 1
solution.js
Напишите функцию generate, которая возвращает указанную строку треугольника паскаля в виде массива.
 */

// console.time('funk');
const start = new Date().getMilliseconds(); // speed test
const generate = (line) => {
  let result = [1];
  for (let i = 0; i < line; i++) {
    let iterResult = [];
    const length = result.length;
    for (let j = 0; j <= length; j++) {
      let upLeft = result[j - 1] || 0;
      let upRigth = result[j] || 0;
      iterResult.push(upLeft + upRigth);
    }
    result = iterResult;
  }
  return result;
};
const end = new Date().getMilliseconds();
console.log(`${end - start}mc`);
// console.timeEnd('funk');
/* speed test 'result.length'
.095
.088
.099
.170
midle res=.113
*/
////////////////////////////////////////
/* speed test 'const length = result.length'
0.095
0.103
0.091
0.192
midle res = .120
*/
/* 
.088 
.0120
////////
.090
0.169*/

// generate(1); // [1, 1]
// generate(4); // [1, 4, 6, 4, 1]
// generate(10000);
