'use strict';
// https://ru.hexlet.io/challenges/js_arrays_longest_substring/instance
/* Реализуйте функцию getLongestLength() принимающую на вход строку и возвращающую длину максимальной последовательности из неповторяющихся символов.
Подстрока может состоять из одного символа. Например в строке qweqrty, можно выделить следующие подстроки: qwe, weqrty.
Самой длинной будет weqrty.
 */

/* const getLongestLength = (string) => {
  const subString = [];
  let count = 0;
  for (const char of string) {
    const index = subString.indexOf(char);
    if (index !== -1) {
      count = count < subString.length ? subString.length : count;
      const numberOfElem = index + 1;
      subString.splice(0, numberOfElem);
    }
    subString.push(char);
  }
  return count < subString.length ? subString.length : count;
}; */
/////////////////////////////////   O(n)
const getLongestLength2 = (str) => {
  const chars = {};
  let res = 0;
  let start = 0;
  const length = str.length;
  for (let i = 0; i < length; i += 1) {
    const char = str[i];
    if (chars.hasOwnProperty(char) && chars[char] >= start) {
      const subLength = i - start;
      res = res > subLength ? res : subLength;
      start = chars[char] + 1;
    }
    chars[char] = i;
  }
  let lastLen = length - start;
  return res > lastLen ? res : lastLen;
};

// getLongestLength2('acbabwer'); // 5 Yarik test
// getLongestLength2('aa'); // 1 Kolya test
// getLongestLength2('abcab'); // 3 Kolya test
// getLongestLength2('abcad'); // 4 Kolya test
// getLongestLength2('abcdeef'); // 5
// getLongestLength2('abbccddeffg'); // 3
// getLongestLength2('jabjcdel'); // 7
// getLongestLength2('abbcdea'); // 5 Kolya test
// getLongestLength2('adebbcdea'); // 5 Kolya test
// getLongestLength(''); // 0
