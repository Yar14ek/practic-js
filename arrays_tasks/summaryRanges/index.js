'use strict';

// https://ru.hexlet.io/challenges/js_arrays_summary_ranges/instance
/* Реализуйте и экспортируйте по умолчанию функцию,
которая находит в массиве непрерывные возрастающие последовательности чисел и возвращает массив с их перечислением. */

const summaryRanges = (numbers) => {
  const result = [];
  let sequenceOfNum = [];
  for (let i = 0; i < numbers.length; i++) {
    sequenceOfNum.push(numbers[i]);
    if (numbers[i] + 1 !== numbers[i + 1]) {
      if (sequenceOfNum.length > 1) {
        result.push(
          `${sequenceOfNum[0]}->${sequenceOfNum[sequenceOfNum.length - 1]}`
        );
      }
      sequenceOfNum = [];
    }
  }
  return result;
};

// summaryRanges([0, 1, 2, 4, 5, 7]);
// ['0->2', '4->5']
summaryRanges([110, 111, 112, 111, -5, -4, -2, -3, -4, -5]);
// ['110->112', '-5->-4']
// summaryRanges([1, 2, 3]);
// ['1->3']
