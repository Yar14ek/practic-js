'use strict';
// https://ru.hexlet.io/challenges/js_arrays_length_of_last/instance
/* Реализуйте и экспортируйте по умолчанию функцию, которая возвращает длину последнего слова переданной на вход строки.
Словом считается любая последовательность, не содержащая пробелов.

Примеры
getLastWordLength(''); // 0


getLastWordLength('hello, world!  '); // 6
 */

// const getLastWordLength = (str) => {
//   const words = str.trim().split(' ');
//   let lastWord = words[words.length - 1];
//   return lastWord.length;
// };

// version if push "war and peace"
const getLastWordLength = (str) => {
  let count = 0;
  for (let i = str.length - 1; i >= 0; i--) {
    if (count > 0 && str.charAt(i) === ' ') {
      break;
    }
    if (str.charAt(i) !== ' ') count++;
  }
  return count;
};

export default getLastWordLength;

// getLastWordLength('hello, wOrLD!  '); //6
// getLastWordLength('blah');
// getLastWordLength('BlacK'); // 5
