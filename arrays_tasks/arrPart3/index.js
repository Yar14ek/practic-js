'use strict';
{
  /* Реализуйте и экспортируйте функцию по умолчанию,
которая принимает на вход строку, состоящую только из открывающих и закрывающих скобок разных типов, 
и проверяет, является ли эта строка сбалансированной.
Открывающие и закрывающие скобки должны быть одного вида. Пустая строка (отсутствие скобок) считается сбалансированной. */
  const openingSymbols = ['(', '[', '{', '<'];
  const closingSymbols = [')', ']', '}', '>'];
  function isBracketStructureBalanced(str) {
    let isBalans = true;
    const stac = [];
    for (const char of str) {
      if (openingSymbols.indexOf(char) !== -1) {
        stac.push(char);
      } else {
        if (
          closingSymbols.indexOf(char) ===
          openingSymbols.indexOf(stac[stac.length - 1])
        ) {
          stac.pop();
        } else {
          isBalans = false;
          break;
        }
      }
    }
    return !stac.length && isBalans ? true : false;
  }
  isBracketStructureBalanced('(<><<<{[()]}>>>)');
}
//////////////////////////////////////////////////////////////////////
{
  /*   Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход два отсортированных массива и находит их пересечение.
  Примеры
  getIntersectionOfSortedArrays([10, 11, 24], [10, 13, 14, 18, 24, 30]); // [10, 24]
  
getIntersectionOfSortedArrays([10, 11, 24], [-2, 3, 4]); // []

getIntersectionOfSortedArrays([], [2]); // []
Алгоритм
Поиск пересечения двух неотсортированных массивов — операция,
в рамках которой выполняется вложенный цикл с полной проверкой каждого элемента первого массива на вхождение во второй.
Сложность данного алгоритма O(n * m) (произведение n и m), где n и m — размерности массивов. Если массивы отсортированы, 
то можно реализовать алгоритм, сложность которого уже O(n + m), что значительно лучше.
Суть алгоритма довольно проста. В коде вводятся два указателя (индекса) на каждый из массивов. 
Начальное значение каждого указателя 0. Затем идёт проверка элементов, находящихся под этими индексами в обоих массивах. 
Если они совпадают, то значение заносится в результирующий массив, а оба индекса инкрементируются. 
Если значение в первом массиве больше, чем во втором, то инкрементируется указатель второго массива, иначе — первого. */
  const getIntersectionOfSortedArrays = (arr1, arr2) => {
    const result = [];
    for (let i = 0; i < arr1.length; i++) {
      let incr = i;
      for (let j = 0; j < arr2.length; j++) {
        if (arr1[i] < arr2[j]) {
          incr += 1;
          break;
        } else if (arr1[i] > arr2[j]) {
          j += 1;
          continue;
        } else if (arr1[i] === arr2[j]) {
          result.push(arr1[i]);
          break;
        }
      }
    }
    return result;
  };

  const getIntersectionOfSortedArrays_2 = (arr1, arr2) => {
    const result = [];
    arr1.forEach((el) => {
      if (arr2.includes(el)) {
        result.push(el);
      }
    });
    return result;
  };
  const getIntersectionOfSortedArrays_3 = (arr1, arr2) =>
    arr1.filter((el) => arr2.includes(el));

  // getIntersectionOfSortedArrays([10, 11, 24], [10, 13, 14, 18, 24, 30]);
  // getIntersectionOfSortedArrays_2([10, 11, 24], [10, 13, 14, 18, 24, 30]);
  // getIntersectionOfSortedArrays_3([10, 11, 24], [10, 13, 14, 18, 24, 30])
}
//////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте функцию getTheNearestLocation(),
  которая находит место ближайшее к указанной точке на карте и возвращает его. Параметры функции:
locations – массив мест на карте. Каждое место это массив из двух элементов, где первый элемент это название места,
второй – точка на карте (массив из двух чисел x и y).
point – текущая точка на карте. Массив из двух элементов-координат x и y.
import { getTheNearestLocation } from '../location.js';
// Если точек нет, то возвращается null
getTheNearestLocation([], currentPoint); // null

getTheNearestLocation(locations, currentPoint); // ['Museum', [8, 4]]
Для решения этой задачи деструктуризация не нужна, но мы хотим её потренировать. Поэтому решите эту задачу без обращения к индексам массивов.

Подсказки
Расстояние между точками высчитывается с помощью функции getDistance. */

  const getDistance = ([, [x1, y1]], [x2, y2]) => {
    return (
      Math.floor(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)) * 100) /
      100
    );
  };

  function getTheNearestLocation(locations, currentPoint) {
    if (!locations.length) {
      return null;
    }
    let point = [];
    const [ferstElem] = locations;
    let minDistanse = getDistance(ferstElem, currentPoint);
    for (const el of locations) {
      const newDist = getDistance(el, currentPoint);
      if (minDistanse > newDist) {
        point = el;
        minDistanse = newDist;
      }
    }
    return point;
  }
  const locations = [
    ['Park', [10, 5]],
    ['Sea', [1, 3]],
    ['Museum', [8, 4]],
  ];
  const currentPoint = [5, 5];
  // getTheNearestLocation(locations, currentPoint);
}
//////////////////////////////////////////////////////////////////////
{
  /*  Реализуйте и экспортируйте функцию getMax(), которая ищет в массиве максимальное значение и возвращает его.
  Эта функция реализуется просто и мы уже делали подобное ранее. Сейчас же мы учимся использовать rest-оператор. 
  Используйте его вместе с деструктуризацией, для извлечения первого элемента и всех остальных. 
  Первый элемент становится начальным значением максимального, 
  а остальные перебираются в цикле для определения максимального. */
  function getMax([ferst, ...rest]) {
    if (!ferst) {
      return null;
    }
    let maxNum = ferst;
    for (const num of rest) {
      if (maxNum < num) {
        maxNum = num;
      }
    }
    return maxNum;
  }
}
//////////////////////////////////////////////////////////////////////
{
  /* Реализуйте и экспортируйте функцию flatten().
  Эта функция принимает на вход массив и выпрямляет его: если элементами массива являются массивы,
  то flatten сводит всё к одному массиву, раскрывая один уровень вложенности.
  Реализуйте добавление элементов вложенного массива в результирующий через spread-оператор. */

  function flatten(arr) {
    let newArr = [];
    for (const elem of arr) {
      if (Array.isArray(elem)) {
        newArr = [...newArr, ...elem];
      } else {
        newArr = [...newArr, elem];
      }
    }
    return newArr;
  }
  // flatten([1, [[2], [3]], [9]])
}
