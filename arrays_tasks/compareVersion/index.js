'use strict';
// https://ru.hexlet.io/challenges/js_arrays_compare_versions/instance
/* solution.js
Реализуйте и экспортируйте по умолчанию функцию,
которая сравнивает переданные версии version1 и version2.
Если version1 > version2, то функция должна вернуть 1, если version1 < version2, то - -1, если же version1 = version2, то - 0.

Версия - это строка, в которой два числа (мажорная и минорные версии) разделены точкой, например: 12.11.
Важно понимать, что версия - это не число с плавающей точкой, а несколько чисел не связанных между собой.
Проверка на больше/меньше производится сравнением каждого числа независимо. Поэтому версия 0.12 больше версии 0.2.

Пример порядка версий:
0.1 < 1.1 < 1.2 < 1.11 < 13.37
Примеры

compareVersion("0.2", "0.1"); // 1

*/

const compareVersion = (ver1, ver2) => {
  const verStToArrNum = (version) => version.split('.').map((el) => Number(el));
  const [major1, minor1] = verStToArrNum(ver1);
  const [major2, minor2] = verStToArrNum(ver2);
  let result = 0;
  if (major1 !== major2) {
    result = major1 > major2 ? 1 : -1;
  } else {
    if (minor1 !== minor2) {
      result = minor1 > minor2 ? 1 : -1;
    }
  }
  return result;
};
compareVersion('0.2', '0.12'); // -1
// compareVersion('4.2', '4.2'); // 0
