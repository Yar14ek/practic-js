'use strict';
// https://ru.hexlet.io/challenges/js_arrays_snail
/* Матрицу можно представить в виде двумерного списка. 
Например, список [[1, 2, 3], [4, 5, 6], [7, 8, 9]] — это отображение матрицы:

1 2 3
4 5 6
7 8 9
snail.js
Реализуйте и экспортируйте по умолчанию функцию,
которая принимает на вход матрицу и возвращает список элементов матрицы по порядку следования от левого
 верхнего элемента по часовой стрелке к внутреннему. Движение по матрице напоминает улитку: */

const buildSnailPath = (matrix) => {
  let copyMatrix = [...matrix];
  let res = [];
  if (copyMatrix.length !== 0) {
    let row = copyMatrix.splice(0, 1)[0];
    res = [row, buildSnailPath(rotateMatrix(copyMatrix))].flat();
  }
  return res;
};

function rotateMatrix(matrix) {
  const newMatrix = [];
  const length = matrix[0] ? matrix[0].length : 0;
  for (let i = 0; i < length; i++) {
    let row = [];
    matrix.forEach((el) => {
      row.push(el[el.length - (1 + i)]);
    });
    newMatrix.push(row);
  }
  return newMatrix;
}
// rotateMatrix([1, 2, 3, 4]);
// console.log(
//   buildSnailPath([
//     [1, 2, 3, 4],
//     [5, 6, 7, 8],
//     [9, 10, 11, 12],
//   ])
// );
// console.log(
//   buildSnailPath([
//     [undefined, '', null],
//     [true, false, 'foo'],
//     [[], {}, { key: 'bar' }],
//   ])
// );
// buildSnailPath([
//   [1, 2, 3, 4],
//   [5, 6, 7, 8],
//   [9, 10, 11, 12],
// ]);
