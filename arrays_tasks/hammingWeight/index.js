'use strict';
// https://ru.hexlet.io/challenges/js_arrays_hamming_weight
/* Вес Хэмминга — это количество единиц в двоичном представлении числа.

solution.js
Реализуйте и экспортируйте по умолчанию функцию, которая считает вес Хэмминга.

Примеры
*/
// const hammingWeight = (num) => {
//   return num
//     .toString(2)
//     .split('')
//     .filter((el) => el === '1').length;
// };

const hammingWeight = (decimal) => {
  let count = 0;
  while (decimal !== 0) {
    if (decimal % 2) count++;
    decimal = Math.floor(decimal / 2);
  }
  return count;
};

// hammingWeight(4); // 1
// hammingWeight(0); // 0
// hammingWeight(101); // 4
