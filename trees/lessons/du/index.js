'use strict';
// https://ru.hexlet.io/courses/js-trees/lessons/calculate/exercise_unit

/* Реализуйте и экспортируйте по умолчанию функцию, которая принимает на вход директорию,
а возвращает список узлов вложенных, (директорий и файлов) в указанную директорию на один уровень, и место которое, они занимают.
Размер файла задается в метаданных. Размер директории складывается из сумм всех размеров файлов,
находящихся внутри во всех поддиректориях. Сами директории размера не имеют.

Пример

const tree = mkdir('/', [
  mkdir('etc', [
    mkdir('apache'),
    mkdir('nginx', [
      mkfile('nginx.conf', { size: 800 }),
    ]),
    mkdir('consul', [
      mkfile('config.json', { size: 1200 }),
      mkfile('data', { size: 8200 }),
      mkfile('raft', { size: 80 }),
    ]),
  ]),
  mkfile('hosts', { size: 3500 }),
  mkfile('resolve', { size: 1000 }),
]);

du(tree);
// [
//   ['etc', 10280],
//   ['hosts', 3500],
//   ['resolve', 1000],
// ]
Примечания
Обратите внимание на структуру результирующего массива. Каждый элемент — массив с двумя значениями: именем директории и размером файлов внутри.
Результат отсортирован по размеру в обратном порядке. То есть сверху самые тяжёлые, внизу самые лёгкие. */
import {
  getChildren,
  mkdir,
  mkfile,
  isFile,
  getName,
  getMeta,
} from '@hexlet/immutable-fs-trees';

const nodeSize = (node) => {
  let nodeSize = 0;
  if (isFile(node)) {
    nodeSize = getMeta(node).size;
  } else {
    nodeSize = getChildren(node)
      .map(nodeSize)
      .reduce((sum, el) => sum + el, 0);
  }
  return nodeSize;
};

const du = (tree) =>
  getChildren(tree)
    .map((child) => [getName(child), nodeSize(child)])
    .sort((a, b) => b[1] - a[1]);
