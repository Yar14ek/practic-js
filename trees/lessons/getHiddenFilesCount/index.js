'use strict';
// https://ru.hexlet.io/courses/js-trees/lessons/aggregation/exercise_unit
/* Реализуйте и экспортируйте по умолчанию функцию, которая считает количество скрытых файлов в директории и всех поддиректориях. Скрытым файлом в Linux системах считается файл, название которого начинается с точки.

Пример
import { mkdir, mkfile } from '@hexlet/immutable-fs-trees';
import getHiddenFilesCount from '../getHiddenFilesCount.js';

const tree = mkdir('/', [
  mkdir('etc', [
    mkdir('apache'),
    mkdir('nginx', [mkfile('.nginx.conf', { size: 800 })]),
    mkdir('.consul', [
      mkfile('.config.json', { size: 1200 }),
      mkfile('data', { size: 8200 }),
      mkfile('raft', { size: 80 }),
    ]),
  ]),
  mkfile('.hosts', { size: 3500 }),
  mkfile('resolve', { size: 1000 }),
]);
getHiddenFilesCount(tree); // 3 */

import { getChildren, isFile, getName } from '@hexlet/immutable-fs-trees';

const getHiddenFilesCount = (tree) => {
  let result = 0;
  if (isFile(tree)) {
    if (getName(tree)[0] === '.') {
      result = 1;
    }
  } else {
    result = getChildren(tree)
      .map(getHiddenFilesCount)
      .reduce((sum, el) => sum + el, 0);
  }
  return result;
};
