'use strict';
// https://ru.hexlet.io/challenges/js_trees_itinerary/instance

/* Реализуйте и экспортируйте по умолчанию функцию, которая выстраивает маршрут между городами.

Функция принимает 3 аргумента:

дерево городов
город старта
город окончания маршрута
и возвращает массив городов, выстроенный в том же порядке, в котором они находятся на пути следования по маршруту.

Примеры
const tree = ['Moscow', [
  ['Smolensk'],
  ['Yaroslavl'],
  ['Voronezh', [
    ['Liski'],
    ['Boguchar'],
    ['Kursk', [
      ['Belgorod', [
        ['Borisovka'],
      ]],
      ['Kurchatov'],
    ]],
  ]],
  ['Ivanovo', [
    ['Kostroma'], ['Kineshma'],
  ]],
  ['Vladimir'],
  ['Tver', [
    ['Klin'], ['Dubna'], ['Rzhev'],
  ]],
]];

itinerary(tree, 'Dubna', 'Kostroma');
// ['Dubna', 'Tver', 'Moscow', 'Ivanovo', 'Kostroma']

itinerary(tree, 'Borisovka', 'Kurchatov');
// ['Borisovka', 'Belgorod', 'Kursk', 'Kurchatov'] */

// const tree = [
//   'Moscow',
//   [
//     ['Smolensk'],
//     ['Yaroslavl'],
//     [
//       'Voronezh',
//       [
//         ['Liski'],
//         ['Boguchar'],
//         ['Kursk', [['Belgorod', [['Borisovka']]], ['Kurchatov']]],
//       ],
//     ],
//     ['Ivanovo', [['Kostroma'], ['Kineshma']]],
//     ['Vladimir'],
//     ['Tver', [['Klin'], ['Dubna'], ['Rzhev']]],
//   ],
// ];

const treeTransform = (tree) => {
  const result = {};
  const itter = (node, parentName = null) => {
    const [point, children] = node;
    const childrenList = children.map((child) => child[0]);
    result[point] = [...childrenList, parentName];
    children?.forEach((el) => itter(el, point));
  };
  itter(tree);
  return result;
};

const itinerary = (tree, startCity, finishCity) => {
  const adjList = treeTransform(tree);
  const itter = (city, path = []) => {
    const children = adjList[city];
    if (children.includes(finishCity)) {
      console.log('finishCity found:>> ', [...path, finishCity]);
      return [...path, finishCity];
    }
    return children
      .flatMap((el) => {
        if (adjList[el] && adjList[el].length > 1 && !path.includes(el)) {
          return itter(el, [...path, el]);
        }
      })
      .filter((el) => el !== undefined);
  };
  return itter(startCity, [startCity]);
};

export default itinerary;
