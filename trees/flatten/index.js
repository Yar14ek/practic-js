'use strict';
// https://ru.hexlet.io/challenges/js_trees_flatten

/* Реализуйте и экспортируйте по умолчанию функцию, которая делает плоским вложенный массив.

Для решения задачи нельзя использовать готовые методы для выравнивания массивов.

Примеры
const list = [1, 2, [3, 5], [[4, 3], 2]];

// [1, 2, 3, 5, 4, 3, 2]
flatten(list); */

const flatten = (arr) => {
  const result = [];
  const itter = (arr) => {
    for (const el of arr) {
      Array.isArray(el) ? itter(el) : result.push(el);
    }
  };
  itter(arr);
  return result;
};

export default flatten;
