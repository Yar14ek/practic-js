// https://ru.hexlet.io/challenges/js_trees_puzzle
/* 
Реализуйте и экспортируйте по умолчанию функцию, которая объединяет отдельные ветки в одно дерево. Каждая из веток в свою очередь является также деревом.

Функция может принимать на вход неограниченное количество веток и соединяет их. Корневым узлом объединённого дерева является корневой узел первой переданной ветки.

Примеры
const branch1 = ['A', [ //   A
  ['B', [               //   |
    ['C'],              //   B
    ['D'],              //  / \
  ]],                   // C   D
]];

const branch2 = ['B', [ //   B
  ['D', [               //   |
    ['E'],              //   D
    ['F'],              //  / \
  ]],                   // E   F
]];

const branch3 = ['I', [ //   I
  ['A', [               //   |
    ['B', [             //   A
      ['C'],            //   |
      ['H'],            //   B
    ]],                 //  / \
  ]],                   // C   H
]];

combine(branch1, branch2, branch3);

// ['A', [      //     A
//   ['B', [    //    / \
//     ['C'],   //   B   I
//     ['D', [  //  /|\
//       ['E'], // C D H
//       ['F'], //  / \
//     ]],      // E   F
//     ['H'],
//   ]],
//   ['I'],
// ]]; */
type Node = [string, Node[]?];
type AdjList = {
  [key: string]: string[]
}

const fromNodesToAdjList = (trees: Node[]): AdjList => {
  const result: AdjList = {}
  const itter = (node: Node, parent?: string): void => {
    const [nodeId, children] = node
    const childrenList = children ? children.map(child => child[0]) : []

    if (!result[nodeId]) {
      result[nodeId] = [];
    }

    result[nodeId].push(...childrenList)

    if (parent) {
      result[nodeId].push(parent)
    }

    if (children) {
      children.forEach(child => itter(child, nodeId))
    }
  }
  trees.forEach(tree => itter(tree))
  for (const key in result) {
    const uniq = new Set(result[key])
    result[key] = Array.from(uniq)
  }
  return result
}

const combine = (...trees: Node[]): Node => {
  const startNodeId = trees[0][0]
  const adjList = fromNodesToAdjList(trees);
  const visitedUnit: Set<string> = new Set()

  const itter = (nodeId: string): Node => {
    visitedUnit.add(nodeId);
    const unVisitedUnit = adjList[nodeId].filter(el => !visitedUnit.has(el));
    return !unVisitedUnit.length ? [nodeId] : [nodeId, unVisitedUnit.map(itter)];
  }

  return itter(startNodeId)
}
const branch1: Node = ['A', [ //   A
  ['B', [               //   |
    ['C'],              //   B
    ['D'],              //  / \
  ]],                   // C   D
]];

const branch2: Node = ['B', [ //   B
  ['D', [               //   |
    ['E'],              //   D
    ['F'],              //  / \
  ]],                   // E   F
]];

const branch3: Node = ['I', [ //   I
  ['A', [               //   |
    ['B', [             //   A
      ['C'],            //   |
      ['H'],            //   B
    ]],                 //  / \
  ]],                   // C   H
]];

combine(branch1, branch2, branch3);
export default combine;