"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transformTreeToObj = (trees) => {
    const result = {};
    const startPoint = trees[0][0];
    const itter = (node, parent) => {
        const [point, children] = node;
        const childrenList = children ? children.map(child => child[0]) : [];
        if (!result[point]) {
            result[point] = [];
        }
        result[point].push(...childrenList);
        if (parent) {
            result[point].push(parent);
        }
        if (children) {
            children.forEach(child => itter(child, point));
        }
    };
    trees.forEach(tree => itter(tree));
    for (const key in result) {
        const unick = new Set(result[key]);
        result[key] = Array.from(unick);
    }
    return result;
};
const combine = (...trees) => {
    const startPoint = trees[0][0];
    const adjList = transformTreeToObj(trees);
    const visitedUnit = new Set();
    const itter = (unit) => {
        visitedUnit.add(unit);
        const unVisitedUnit = adjList[unit].filter(el => !visitedUnit.has(el));
        return !unVisitedUnit.length ? [unit] : [unit, unVisitedUnit.map(itter)];
    };
    return itter(startPoint);
};
const branch1 = ['A', [
        ['B', [
                ['C'],
                ['D'],
            ]],
    ]];
const branch2 = ['B', [
        ['D', [
                ['E'],
                ['F'],
            ]],
    ]];
const branch3 = ['I', [
        ['A', [
                ['B', [
                        ['C'],
                        ['H'],
                    ]],
            ]],
    ]];
combine(branch1, branch2, branch3);
exports.default = combine;
//# sourceMappingURL=index.js.map