'use strict';
// https://ru.hexlet.io/challenges/js_data_abstraction_url/instance

/* Реализуйте абстракцию для работы с урлами. Она должна извлекать и менять части адреса. Интерфейс:

make(url) - Конструктор. Создает урл.***
setProtocol(data, protocol) - Сеттер. Меняет схему.***
getProtocol(data) - Селектор (геттер). Извлекает схему.***
setHost(data, host) - Сеттер. Меняет хост.
getHost(data) - Геттер. Извлекает хост.
setPath(data, path) - Сеттер. Меняет строку запроса.
getPath(data) - Геттер. Извлекает строку запроса.
setQueryParam(data, key, value) - Сеттер. Устанавливает значение для параметра запроса.
getQueryParam(data, paramName, def = null) - Геттер. Извлекает значение для параметра запроса. Третьим параметром функция принимает значение по умолчанию, которое возвращается тогда, когда в запросе не было такого параметра
toString(data) - Геттер. Преобразует урл в строковой вид. */

const make = (url) => new URL(url);

const setProtocol = (data, protocol) => (data.protocol = protocol);

const getProtocol = (data) => data.protocol;

const setHost = (data, host) => (data.host = host);

const getHost = (data) => data.host;

const setPath = (data, path) => (data.pathname = path);

const getPath = (data) => data.pathname;

const toString = (data) => data.toString();

const setQueryParam = (data, key, value) => data.searchParams.set(key, value);

const getQueryParam = (data, paramName, defaultValue = null) =>
  data.searchParams.get(paramName) || defaultValue;

export {
  make,
  getProtocol,
  getHost,
  getPath,
  setProtocol,
  setHost,
  setPath,
  getQueryParam,
  setQueryParam,
  toString,
};
