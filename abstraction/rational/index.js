'use strict';

// https://ru.hexlet.io/courses/js-data-abstraction/lessons/invariants/exercise_unit
/* Реализуйте абстракцию для работы с рациональными числами включающую в себя следующие функции:

Конструктор makeRational() - принимает на вход числитель и знаменатель, возвращает дробь.
Селектор getNumer() - возвращает числитель
Селектор getDenom() - возвращает знаменатель
Сложение add() - складывает переданные дроби
Вычитание sub() - находит разность между двумя дробями
Не забудьте реализовать нормализацию дробей удобным для вас способом.

const rat1 = makeRational(3, 9); */

const getGcd = (a, b) => (a % b ? getGcd(b, a % b) : Math.abs(b));

const makeRational = (num, den) => {
  const dif = getGcd(num, den);
  return { numer: num / dif, denom: den / dif };
};

const getNumer = (rat) => rat.numer;
const getDenom = (rat) => rat.denom;

const calck = (rations, operator) => {
  const denoms = rations.map((rat) => rat.denom);
  const commonDenom = denoms.reduce((a, b) => a * b);
  const summNum = rations
    .map(({ numer, denom }) => numer * (commonDenom / denom))
    .reduce((a, b) => (operator === '+' ? a + b : a - b));
  return makeRational(summNum, commonDenom);
};

const add = (...args) => calck(args, '+');
const sub = (...args) => calck(args, '-');

const ratToString = (rat) => `${getNumer(rat)}/${getDenom(rat)}`;
